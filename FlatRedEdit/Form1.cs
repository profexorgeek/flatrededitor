﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FlatRedEditor;
using FlatRedBall;
using Timer = System.Timers.Timer;
using System.Reflection;
using System.IO;

namespace FlatRedEdit {
    public partial class Form1 : Form {

        private Game1 mGameInstance;
        private Timer mGameTimer;
        private bool mWasTimerEnabled;
        private Assembly mProjectAssembly;
        private bool mAssemblyLoaded;
        private List<Type> mProjectEntityTypes;
        private List<PositionedObject> mAddedObjects;

        public Form1() {
            SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint, true);
            UpdateStyles();
            InitializeComponent();

            // create a game instance and pass the game timer, which starts when Initialization is complete
            mGameInstance = new Game1(() => {
                mGameTimer = new Timer(15);
                mGameTimer.Elapsed += (sender, e) => {
                    try {
                        mGameInstance.Tick();
                    }
                    catch (Exception ex) {
                        Console.WriteLine("Failed to tick, game not set up? " + ex.Message);
                    }
                };
                mGameTimer.Start();
            });

            // get a handle from the GameInstance to use as a Form and stuff it in the Viewport Panel we created
            Form form = Form.FromHandle(mGameInstance.Window.Handle) as Form;
            form.FormBorderStyle = FormBorderStyle.None;
            form.Dock = DockStyle.Fill;
            form.TopLevel = false;
            form.Parent = Viewport;
            Viewport.Controls.Add(form);
            form.Visible = true;
            form.MouseEnter += new EventHandler(Viewport_MouseEnter);
            form.MouseLeave += new EventHandler(Viewport_MouseLeave);

            UpdateViewportResolution();

            mAssemblyLoaded = false;
            mAddedObjects = new List<PositionedObject>();
        }

        private void LoadAssemblyEntities(string path)
        {
            mProjectAssembly = Assembly.LoadFile(path);
            IEnumerable<Type> query = from type in mProjectAssembly.GetTypes()
                                      where type.IsClass && type.Namespace.Contains("Entities") && type.IsSubclassOf(typeof(FlatRedBall.PositionedObject))
                                      select type;
            mProjectEntityTypes = query.ToList();
            if (mProjectEntityTypes.Count == 0)
            {
                MessageBox.Show("No Entities were found in this assembly!");
            }
            else
            {
                for (int i = 0; i < mProjectEntityTypes.Count; i++)
                {
                    listEntities.Items.Add(mProjectEntityTypes[i]);
                }
                mAssemblyLoaded = true;
                MessageBox.Show(string.Format("{0} Entities loaded.", mProjectEntityTypes.Count));
            }
        }

        private void UpdateViewportResolution()
        {
            FlatRedBallServices.GraphicsOptions.SuspendDeviceReset();
            FlatRedBallServices.GraphicsOptions.SetResolution(Viewport.Width, Viewport.Height);
            SpriteManager.Camera.UsePixelCoordinates();
            FlatRedBallServices.GraphicsOptions.ResumeDeviceReset();
            FlatRedBallServices.GraphicsOptions.ResetDevice();
        }

        protected override void Dispose(bool disposing)
        {
            mGameTimer.Stop();
            mGameInstance.Dispose();
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        

        #region Form EventHandlers
        private void btnEnableRender_Click(object sender, EventArgs e)
        {
            if (mGameTimer.Enabled)
            {
                btnEnableRender.Text = "Play";
                mGameTimer.Enabled = false;
            }
            else
            {
                btnEnableRender.Text = "Pause";
                mGameTimer.Enabled = true;
            }
        }


        private void Form1_ResizeBegin(object sender, EventArgs e)
        {
            // pause game engine while resizing or Draw() calls will blow up
            // stores reference to current timer state
            mWasTimerEnabled = mGameTimer.Enabled;
            mGameTimer.Enabled = false;
        }

        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            // resume game timer state and update resolution after resize
            UpdateViewportResolution();
            mGameTimer.Enabled = mWasTimerEnabled;
        }

        private void loadProjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Title = "Open Project Executable";
            dialog.Filter = "exe files (*.exe)|*.exe";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string assemblyPath = dialog.FileName;
                FlatRedBall.IO.FileManager.RelativeDirectory = Path.GetDirectoryName(assemblyPath);
                if (File.Exists(assemblyPath))
                {
                    LoadAssemblyEntities(assemblyPath);
                }
                else
                {
                    MessageBox.Show("Specified file did not exist.");
                }

            }
            else
            {
                MessageBox.Show("Unable to load project.");
            }
            dialog.Dispose();
        }

        private void Viewport_MouseEnter(object sender, EventArgs e)
        {
            Game1.HasFocus = true;
        }

        private void Viewport_MouseLeave(object sender, EventArgs e)
        {
            Game1.HasFocus = false;
        }
        #endregion

        private void btnAddEntity_Click(object sender, EventArgs e)
        {
            if (!mAssemblyLoaded || mProjectEntityTypes == null || mProjectEntityTypes.Count == 0)
            {
                MessageBox.Show("There are no loaded entities. Load a project exe from the File menu to add entities.");
            }
            else
            {
                Type objectType = (Type)listEntities.SelectedItem;
                string contentManagerName = FlatRedEditor.Screens.ScreenManager.CurrentScreen.ContentManagerName;
                PositionedObject instance = (PositionedObject)Activator.CreateInstance(objectType, new object[] {contentManagerName});
                mAddedObjects.Add(instance);
            }
        }
    }
}
