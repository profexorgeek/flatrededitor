﻿namespace FlatRedEdit {
    partial class Form1 {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.Viewport = new System.Windows.Forms.Panel();
            this.btnEnableRender = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadProjectToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ObjectData = new System.Windows.Forms.PropertyGrid();
            this.TabLayout = new System.Windows.Forms.TabControl();
            this.TabProjectResources = new System.Windows.Forms.TabPage();
            this.TabProperties = new System.Windows.Forms.TabPage();
            this.listEntities = new System.Windows.Forms.ListBox();
            this.lblEntities = new System.Windows.Forms.Label();
            this.btnAddEntity = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.TabLayout.SuspendLayout();
            this.TabProjectResources.SuspendLayout();
            this.TabProperties.SuspendLayout();
            this.SuspendLayout();
            // 
            // Viewport
            // 
            this.Viewport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Viewport.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Viewport.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Viewport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Viewport.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Viewport.Location = new System.Drawing.Point(451, 27);
            this.Viewport.Name = "Viewport";
            this.Viewport.Size = new System.Drawing.Size(545, 662);
            this.Viewport.TabIndex = 0;
            // 
            // btnEnableRender
            // 
            this.btnEnableRender.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEnableRender.Location = new System.Drawing.Point(921, 695);
            this.btnEnableRender.Name = "btnEnableRender";
            this.btnEnableRender.Size = new System.Drawing.Size(75, 23);
            this.btnEnableRender.TabIndex = 2;
            this.btnEnableRender.Text = "Pause";
            this.btnEnableRender.UseVisualStyleBackColor = true;
            this.btnEnableRender.Click += new System.EventHandler(this.btnEnableRender_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadProjectToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadProjectToolStripMenuItem
            // 
            this.loadProjectToolStripMenuItem.Name = "loadProjectToolStripMenuItem";
            this.loadProjectToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.loadProjectToolStripMenuItem.Text = "Load Project";
            this.loadProjectToolStripMenuItem.Click += new System.EventHandler(this.loadProjectToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // ObjectData
            // 
            this.ObjectData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ObjectData.HelpVisible = false;
            this.ObjectData.Location = new System.Drawing.Point(0, 0);
            this.ObjectData.Name = "ObjectData";
            this.ObjectData.Size = new System.Drawing.Size(425, 665);
            this.ObjectData.TabIndex = 4;
            // 
            // TabLayout
            // 
            this.TabLayout.Controls.Add(this.TabProjectResources);
            this.TabLayout.Controls.Add(this.TabProperties);
            this.TabLayout.Location = new System.Drawing.Point(12, 27);
            this.TabLayout.Multiline = true;
            this.TabLayout.Name = "TabLayout";
            this.TabLayout.SelectedIndex = 0;
            this.TabLayout.Size = new System.Drawing.Size(433, 691);
            this.TabLayout.TabIndex = 5;
            // 
            // TabProjectResources
            // 
            this.TabProjectResources.Controls.Add(this.btnAddEntity);
            this.TabProjectResources.Controls.Add(this.lblEntities);
            this.TabProjectResources.Controls.Add(this.listEntities);
            this.TabProjectResources.Location = new System.Drawing.Point(4, 22);
            this.TabProjectResources.Name = "TabProjectResources";
            this.TabProjectResources.Padding = new System.Windows.Forms.Padding(3);
            this.TabProjectResources.Size = new System.Drawing.Size(425, 665);
            this.TabProjectResources.TabIndex = 0;
            this.TabProjectResources.Text = "Project";
            this.TabProjectResources.UseVisualStyleBackColor = true;
            // 
            // TabProperties
            // 
            this.TabProperties.Controls.Add(this.ObjectData);
            this.TabProperties.Location = new System.Drawing.Point(4, 22);
            this.TabProperties.Name = "TabProperties";
            this.TabProperties.Padding = new System.Windows.Forms.Padding(3);
            this.TabProperties.Size = new System.Drawing.Size(425, 665);
            this.TabProperties.TabIndex = 1;
            this.TabProperties.Text = "Properties";
            this.TabProperties.UseVisualStyleBackColor = true;
            // 
            // listEntities
            // 
            this.listEntities.FormattingEnabled = true;
            this.listEntities.Location = new System.Drawing.Point(6, 23);
            this.listEntities.Name = "listEntities";
            this.listEntities.Size = new System.Drawing.Size(365, 199);
            this.listEntities.TabIndex = 0;
            // 
            // lblEntities
            // 
            this.lblEntities.AutoSize = true;
            this.lblEntities.Location = new System.Drawing.Point(6, 7);
            this.lblEntities.Name = "lblEntities";
            this.lblEntities.Size = new System.Drawing.Size(135, 13);
            this.lblEntities.TabIndex = 1;
            this.lblEntities.Text = "Entities (PositionedObjects)";
            // 
            // btnAddEntity
            // 
            this.btnAddEntity.Location = new System.Drawing.Point(377, 23);
            this.btnAddEntity.Name = "btnAddEntity";
            this.btnAddEntity.Size = new System.Drawing.Size(42, 199);
            this.btnAddEntity.TabIndex = 2;
            this.btnAddEntity.Text = ">>";
            this.btnAddEntity.UseVisualStyleBackColor = true;
            this.btnAddEntity.Click += new System.EventHandler(this.btnAddEntity_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.TabLayout);
            this.Controls.Add(this.btnEnableRender);
            this.Controls.Add(this.Viewport);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(1024, 768);
            this.MinimumSize = new System.Drawing.Size(1024, 768);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FlatRedEditor";
            this.ResizeBegin += new System.EventHandler(this.Form1_ResizeBegin);
            this.ResizeEnd += new System.EventHandler(this.Form1_ResizeEnd);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.TabLayout.ResumeLayout(false);
            this.TabProjectResources.ResumeLayout(false);
            this.TabProjectResources.PerformLayout();
            this.TabProperties.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Viewport;
        private System.Windows.Forms.Button btnEnableRender;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadProjectToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.PropertyGrid ObjectData;
        private System.Windows.Forms.TabControl TabLayout;
        private System.Windows.Forms.TabPage TabProjectResources;
        private System.Windows.Forms.TabPage TabProperties;
        private System.Windows.Forms.Label lblEntities;
        private System.Windows.Forms.ListBox listEntities;
        private System.Windows.Forms.Button btnAddEntity;
    }
}

