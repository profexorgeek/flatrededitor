using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using FlatRedBall;
using FlatRedBall.Graphics;
using FlatRedBall.Utilities;

using FlatRedEditor.Screens;

namespace FlatRedEditor
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        public GraphicsDeviceManager graphics;
        public ContentManager content;
        public static bool HasFocus = false;

        // custom game class takes an Action as a callback for when we're ready to run
        public Game1(Action onInitializeComplete)
        {
            graphics = new GraphicsDeviceManager(this);
            content = new ContentManager(Services);
            
            // give the base class a reference to our graphics manager
            IGraphicsDeviceManager graphicsDeviceManager = (IGraphicsDeviceManager)graphics; 
            if (graphicsDeviceManager != null) {
                graphicsDeviceManager.CreateDevice();
                this.GetType().BaseType.GetField("graphicsDeviceManager",
                    System.Reflection.BindingFlags.Instance |
                    System.Reflection.BindingFlags.NonPublic).SetValue(this, graphicsDeviceManager);
            }

            // standard FRB setup
            Content.RootDirectory = "Content";
			BackStack<string> bs = new BackStack<string>();
			bs.Current = string.Empty;

            // pass the callback to our Initilization
            Initialize(onInitializeComplete);
        }

        protected void Initialize(Action onInitializeComplete)
        {
            // manually call the base Initialize and begin the game running
            base.Initialize();
            this.BeginRun();

            // fairly standard FRB init with 2D camera
            Renderer.UseRenderTargets = false;
            FlatRedBallServices.InitializeFlatRedBall(this, graphics);
            FlatRedBallServices.IsWindowsCursorVisible = true;
            SpriteManager.Camera.UsePixelCoordinates();

            // get the GameTime property from the base Game class
            GameTime gt = this.GetType().BaseType.GetField("gameTime",
                System.Reflection.BindingFlags.Instance |
                System.Reflection.BindingFlags.NonPublic).GetValue(this) as GameTime;

            // manually call the first Update
            this.Update(gt);

            // notify base class that the first update has been completed
            this.GetType().BaseType.GetField("doneFirstUpdate",
                System.Reflection.BindingFlags.Instance |
                System.Reflection.BindingFlags.NonPublic).SetValue(this, true);

            // navigate to our starting screen
            Screens.ScreenManager.Start(typeof(FlatRedEditor.Screens.Editor).FullName);

            // we should be ready to run, fire the callback
            onInitializeComplete();
        }

        protected override void Update(GameTime gameTime)
        {
            if (!HasFocus) {
                // disable input here
            }
            FlatRedBallServices.Update(gameTime);
            ScreenManager.Activity();
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            FlatRedBallServices.Draw();
            base.Draw(gameTime);
        }

        protected override void Dispose(bool disposing) {
            this.EndRun();
            base.Dispose(disposing);
        }
    }
}
